package FirstJavaPack;

interface Drawable{  //interface
void draw();  
}  
//Implementation: by second user  
class Rectangle implements Drawable{ //super class 
public void draw()
{
	System.out.println("drawing rectangle");
}  
}  
class Circle extends Rectangle//subclass
{  
public void draw()
{
	System.out.println("drawing circle");
}  
}  
//Using interface: by third user  
class TestInterface1{ //main class 
public static void main(String args[]){ 
	System.out.println("Basic geometrics drawing");
Drawable d=new Circle();  
d.draw(); 
Drawable r=new Rectangle();
r.draw();
}
}  